//Assignment 3- Temperature and Humidity display
//Developed by bendikz, tommylb, erminz, farhata

// Libraries
#include <Arduino.h>
#include <HTS221Sensor.h>
#include "DFRobot_RGBLCD.h"

//I2C port definition
#define I2C2_SCL    PB10
#define I2C2_SDA    PB11

// I2C
TwoWire dev_i2c(I2C2_SDA, I2C2_SCL);

// Components.
DFRobot_RGBLCD lcd(16,2);//connections: 3.3v, gnd, d14, d15
HTS221Sensor  HumTemp(&dev_i2c);

//Interrupt function
void blink();



void setup() {

  pinMode(PC13, INPUT_PULLUP); //User button on controller board
  pinMode(LED3, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(A2, OUTPUT); //Speaker output, A2-GND with series resistor
  delay(1); //Stabilizer delay

  attachInterrupt(PC13, blink, FALLING);

//Speaker Beep
int duration=300;
while(duration>0){
digitalWrite(A2, HIGH);
delay(1);
digitalWrite(A2, LOW);
delay(1);
duration--;
};
int duration2=75;
while(duration2>0){
digitalWrite(A2, HIGH);
delay(3);
digitalWrite(A2, LOW);
delay(3);
duration2--;
};
int duration1=150;
while(duration1>0){
digitalWrite(A2, HIGH);
delay(2);
digitalWrite(A2, LOW);
delay(2);
duration1--;
};


  //Initialize display
  lcd.init();
  
  // Initialize serial TX
  Serial.begin(9600);
  Serial.println("Serial TX Initialized");
  lcd.print("Serial TX Init.");
  delay(500);
  

  // Initialize I2C bus
  dev_i2c.begin();

  // Initlialize Humidity/Temp sensor
  HumTemp.begin();
  HumTemp.Enable();

 //Startup Info scroll
 lcd.setRGB(255, 0, 255);

    lcd.clear();
    lcd.print("Assignment_3");
    delay(1000);
    lcd.clear();

    delay(10);
    lcd.setRGB(225, 225, 255);
    lcd.print("Air Monitor Boi");
    delay(1000);
    lcd.clear();
    
    delay(10);
    lcd.setRGB(255, 255, 0);
    lcd.print("By IKT104 grp 16");
    delay(1000);
     lcd.clear();
    
    delay(10);
    lcd.setRGB(255, 0, 0);
    lcd.print("Psh btn 2 select");
    delay(1000);
    lcd.clear();
    
    lcd.print("Temp/humidity");
    delay(1000);
    lcd.clear();


    lcd.print("LET'S");
    
    lcd.setRGB(0, 0, 255);
    delay(100);
    lcd.setRGB(255, 0, 0);delay(100);
    lcd.setRGB(0, 0, 255);delay(100);
    lcd.setRGB(255, 0, 0);delay(100);
    lcd.setRGB(0, 0, 255);delay(100);
    lcd.setRGB(255, 0, 0);delay(100);lcd.setRGB(0, 0, 255);delay(100);
    lcd.setRGB(255, 0, 0);delay(100);lcd.setRGB(0, 0, 255);delay(100);
    lcd.setRGB(255, 0, 0);delay(100);lcd.setRGB(0, 0, 255);delay(100);
    lcd.setRGB(255, 0, 0);delay(100);
    lcd.setRGB(0, 0, 255);
    //lcd.clear();

    lcd.print(" FUCKING");
    
    lcd.setRGB(0, 0, 255);
    delay(100);
    lcd.setRGB(255, 0, 0);delay(100);
    lcd.setRGB(0, 0, 255);delay(100);
    lcd.setRGB(255, 0, 0);delay(100);
    lcd.setRGB(0, 0, 255);delay(100);
    lcd.setRGB(255, 0, 0);delay(100);lcd.setRGB(0, 0, 255);delay(100);
    lcd.setRGB(255, 0, 0);delay(100);lcd.setRGB(0, 0, 255);delay(100);
    lcd.setRGB(255, 0, 0);delay(100);lcd.setRGB(0, 0, 255);delay(100);
    lcd.setRGB(255, 0, 0);delay(100);
    lcd.setRGB(0, 0, 255);
    lcd.clear();

    lcd.print("GOOOOOOOOOOOOOOO");
    
    lcd.setRGB(0, 0, 255);
    delay(100);
    lcd.setRGB(255, 0, 0);delay(100);
    lcd.setRGB(0, 0, 255);delay(100);
    lcd.setRGB(255, 0, 0);delay(100);
    lcd.setRGB(0, 0, 255);delay(100);
    lcd.setRGB(255, 0, 0);delay(100);lcd.setRGB(0, 0, 255);delay(100);
    lcd.setRGB(255, 0, 0);delay(100);lcd.setRGB(0, 0, 255);delay(100);
    lcd.setRGB(255, 0, 0);delay(100);lcd.setRGB(0, 0, 255);delay(100);
    lcd.setRGB(255, 0, 0);delay(100);
    lcd.setRGB(0, 0, 255);
    lcd.clear();


    //Speaker beep
    int duration3=40;
while(duration3>0){
digitalWrite(A2, HIGH);
delay(3);
digitalWrite(A2, LOW);
delay(3);
duration3--;
};
int duration4=80;
while(duration4>0){
digitalWrite(A2, HIGH);
delay(2);
digitalWrite(A2, LOW);
delay(2);
duration4--;
};



    delay(1000);






}

//Interrupt handling
volatile byte mode = LOW;

//Status buzzer function, just for giggs and shittles




void loop() {





  //Status indicators
  digitalWrite(LED_BUILTIN, HIGH);// Blink LED1 for each loop iteration, indicating any hangups -or lack thereof.
  delay(50);
  digitalWrite(LED_BUILTIN, LOW);
  delay(1);

  digitalWrite(LED3, mode);//Indicate current display mode with internal LED3,4 indicator (BLUE=Temp, YELLOW=Humidity)
  delay(1);
  //Read HumTemp
  float humidity;
    HumTemp.GetHumidity(&humidity);
  float temperature;
    HumTemp.GetTemperature(&temperature);
  
  
  

    //Display humidity
    if(mode==HIGH){
    
    
    lcd.clear();
    delay(1);
    
    
    //Serial.print("Hum[%]: ");
    //Serial.print(humidity, 2);

    //lcd.setRGB(255, 255, 255);
    lcd.print("Humidity: ");
    lcd.print(humidity, 2);
    lcd.print("%");
    
    
    int lcd_hum = (255-(humidity*2.55));//Backlight intensity variable depending on humidity(Dry, White - Moist, Blue)
    lcd.setRGB(lcd_hum, lcd_hum, 255);//Reduce green and red when humidity increases
    
    delay(1500);
    }
    


    //Display temperature
    else{

    lcd.clear();  
    delay(1);
    
    
    //Serial.print("Temp[C]: ");
    //Serial.println(temperature, 2);

    lcd.print("Temp: ");
    lcd.print(temperature, 2);
    lcd.print(" C");
    
    
  if(temperature<20){
  lcd.setRGB(10, 10, 255);//LCD backlight color depending on temperature, blue when t<20C
  }
  else if(temperature>25){  //Red when t>25C
   lcd.setRGB(255, 10, 10); 
  }
  else{                     //Orange for all else(20-25C) 
  lcd.setRGB(255, 122, 10);
  };


//Serial prints
    Serial.print("Temp[C]: ");
    Serial.print(temperature, 2);
    
    Serial.print(" Hum[%]: ");
    Serial.println(humidity, 2);
    
    


delay(1500);


};


}

void blink() {
  mode = !mode;


  
  }                        //Ext. Interrupt handler